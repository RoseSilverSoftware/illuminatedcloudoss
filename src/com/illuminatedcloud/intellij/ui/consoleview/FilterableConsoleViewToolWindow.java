/*
 * Copyright 2015-2016 Rose Silver Software LLC and Scott Wells
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.illuminatedcloud.intellij.ui.consoleview;

import com.intellij.execution.filters.OpenFileHyperlinkInfo;
import com.intellij.execution.ui.ConsoleViewContentType;
import com.intellij.openapi.application.PathManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Disposer;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.JComponent;
import java.io.File;
import java.io.IOException;

public class FilterableConsoleViewToolWindow
{
    private static final String ID = "Filterable Console View";

    private final FilterableConsoleView filterableConsoleView;

    public FilterableConsoleViewToolWindow(@NotNull Project project)
    {
        filterableConsoleView = new FilterableConsoleView(project);
    }

    @NotNull
    public FilterableConsoleView getFilterableConsoleView()
    {
        return filterableConsoleView;
    }

    @NotNull
    public JComponent getComponent()
    {
        return filterableConsoleView.getComponent();
    }

    @NotNull
    public JComponent getPreferredFocusableComponent()
    {
        return filterableConsoleView.getPreferredFocusableComponent();
    }

    @Nullable
    public static Content newTab(@NotNull Project project, @NotNull String displayName)
    {
        ToolWindowManager toolWindowManager = ToolWindowManager.getInstance(project);
        if (toolWindowManager != null)
        {
            ToolWindow toolWindow = toolWindowManager.getToolWindow(ID);
            if (toolWindow != null)
            {
                final ContentManager contentManager = toolWindow.getContentManager();
                if (contentManager != null)
                {
                    FilterableConsoleViewToolWindow filterableConsoleViewToolWindow = new FilterableConsoleViewToolWindow(project);
                    final Content content = contentManager.getFactory().createContent(filterableConsoleViewToolWindow.getComponent(), displayName, /*isLockable=*/true);
                    content.setPreferredFocusableComponent(filterableConsoleViewToolWindow.getPreferredFocusableComponent());
                    content.setCloseable(false);
                    content.setDisplayName(displayName);
                    content.setDescription(ID);
                    Disposer.register(project, content);

                    toolWindow.show(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            contentManager.addContent(content);
                            contentManager.setSelectedContentCB(content, /*requestFocus=*/true);
                        }
                    });

                    // Throw the contents of idea.log into the console view to demonstrate its behavior
                    addSampleContent(filterableConsoleViewToolWindow.getFilterableConsoleView());

                    return content;
                }
            }
        }

        return null;
    }

    private static void addSampleContent(@NotNull FilterableConsoleView filterableConsoleView)
    {
        String logPath = PathManager.getLogPath();
        File logFile = new File(logPath, "idea.log");
        if (logFile.canRead())
        {
            Project project = filterableConsoleView.getProject();

            // NOTE: allowFiltering=false ensures that this entry is always present even when a filter is applied
            filterableConsoleView.print("Contents of: ", ConsoleViewContentType.SYSTEM_OUTPUT, /*allowFiltering=*/false);
            // NOTE: Also show hyperlinked entries
            VirtualFile virtualLogFile = VfsUtil.findFileByIoFile(logFile, false);
            if (virtualLogFile != null)
            {
                filterableConsoleView.printHyperlink(logFile.getAbsolutePath(), new OpenFileHyperlinkInfo(project, virtualLogFile, 1), /*allowFiltering=*/false);
            }
            else
            {
                filterableConsoleView.print(logFile.getAbsolutePath(), ConsoleViewContentType.SYSTEM_OUTPUT, /*allowFiltering=*/false);
            }
            filterableConsoleView.print("\n\n", ConsoleViewContentType.SYSTEM_OUTPUT, /*allowFiltering=*/false);

            try
            {
                String logContents = FileUtil.loadFile(logFile);
                String[] logLines = StringUtil.splitByLinesKeepSeparators(logContents);
                for (String logLine : logLines)
                {
                    filterableConsoleView.print(logLine, ConsoleViewContentType.NORMAL_OUTPUT);
                }
            }
            catch (IOException e)
            {
                filterableConsoleView.print(e.getMessage() + "\n", ConsoleViewContentType.ERROR_OUTPUT, /*allowFiltering=*/false);
            }
        }
        else
        {
            filterableConsoleView.print("Unable to read contents of " + logFile.getPath(), ConsoleViewContentType.ERROR_OUTPUT);
        }

        filterableConsoleView.scrollTo(0);
    }
}
