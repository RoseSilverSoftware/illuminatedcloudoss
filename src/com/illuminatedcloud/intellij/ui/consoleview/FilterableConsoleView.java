/*
 * Copyright 2015-2016 Rose Silver Software LLC and Scott Wells
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.illuminatedcloud.intellij.ui.consoleview;

import com.illuminatedcloud.util.Filter;
import com.intellij.execution.filters.HyperlinkInfo;
import com.intellij.execution.impl.ConsoleViewImpl;
import com.intellij.execution.process.ProcessHandler;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.execution.ui.ConsoleViewContentType;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Disposer;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.ui.DocumentAdapter;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FilterableConsoleView implements ConsoleView
{
    private JPanel rootPanel;
    private JLabel filterLabel;
    private JTextField filterTextField;
    private JPanel headerPanel;
    private JPanel consoleViewPanel;

    private ConsoleViewImpl consoleView;

    private final Project project;
    private final List<ConsoleViewEntry> consoleViewEntries = new LinkedList<ConsoleViewEntry>();

    public FilterableConsoleView(@NotNull Project project)
    {
        this.project = project;
        $$$setupUI$$$();
        initialize();
    }

    public void initialize()
    {
        filterLabel.setDisplayedMnemonic(KeyEvent.VK_F);
        filterTextField.getDocument().addDocumentListener(new DocumentAdapter()
        {
            @Override
            protected void textChanged(DocumentEvent documentEvent)
            {
                updateConsoleView();
            }
        });

        consoleView = new ConsoleViewImpl(project, /*viewer=*/true);
        consoleViewPanel.add(consoleView.getComponent(), BorderLayout.CENTER);
        Disposer.register(project, consoleView);
    }

    @NotNull
    public Project getProject()
    {
        return project;
    }

    @Override
    public JComponent getComponent()
    {
        // Make sure the console component is initialized
        consoleView.getComponent();

        // But return the root panel for this view
        return rootPanel;
    }

    public void clear()
    {
        consoleViewEntries.clear();
        consoleView.clear();
    }

    public void print(@NotNull String message, @NotNull ConsoleViewContentType contentType)
    {
        String[] messageLines = StringUtil.splitByLinesKeepSeparators(message);
        for (String messageLine : messageLines)
        {
            print(messageLine, contentType, true);
        }
    }

    public void print(@NotNull String message, @NotNull ConsoleViewContentType contentType, boolean allowFiltering)
    {
        String[] messageLines = StringUtil.splitByLinesKeepSeparators(message);
        for (String messageLine : messageLines)
        {
            printRaw(messageLine, contentType, allowFiltering);
        }
    }

    private void printRaw(@NotNull String messageLine, @NotNull ConsoleViewContentType contentType, boolean allowFiltering)
    {
        ConsoleViewEntry consoleViewEntry = new ConsoleViewEntry(messageLine, contentType, allowFiltering);
        consoleViewEntries.add(consoleViewEntry);
        printLogEntry(consoleViewEntry);
    }

    @SuppressWarnings("unused")
    public void printHyperlink(@NotNull String message, @NotNull HyperlinkInfo hyperlinkInfo)
    {
        printHyperlink(message, hyperlinkInfo, true);
    }

    public void printHyperlink(@NotNull String message, @NotNull HyperlinkInfo hyperlinkInfo, boolean allowFiltering)
    {
        String[] messageLines = StringUtil.splitByLinesKeepSeparators(message);
        for (String messageLine : messageLines)
        {
            ConsoleViewEntry consoleViewEntryEntry = new ConsoleViewEntry(messageLine, hyperlinkInfo, allowFiltering);
            consoleViewEntries.add(consoleViewEntryEntry);
            printLogEntry(consoleViewEntryEntry);
        }
    }

    private void updateConsoleView()
    {
        consoleView.clear();

        for (ConsoleViewEntry consoleViewEntryEntry : consoleViewEntries)
        {
            printLogEntry(consoleViewEntryEntry);
        }
    }

    private void printLogEntry(@NotNull ConsoleViewEntry consoleViewEntryEntry)
    {
        Filter<String> filter = null;
        final String filterText = filterTextField.getText();
        if (StringUtil.isNotEmpty(filterText))
        {
            filter = new Filter<String>()
            {
                @Override
                public boolean accept(String message)
                {
                    Pattern filterPattern = Pattern.compile("(?i)" + filterText);
                    Matcher filterMatcher = filterPattern.matcher(message);
                    return filterMatcher.find();
                }
            };
        }

        boolean allowFiltering = consoleViewEntryEntry.isAllowFiltering();
        String message = consoleViewEntryEntry.getMessage();
        if (!allowFiltering || ((filter == null) || filter.accept(message)))
        {
            ConsoleViewContentType contentType = consoleViewEntryEntry.getContentType();
            HyperlinkInfo hyperlinkInfo = consoleViewEntryEntry.getHyperlinkInfo();
            if (hyperlinkInfo != null)
            {
                consoleView.printHyperlink(message, hyperlinkInfo);
            }
            else
            {
                consoleView.print(message, contentType);
            }
        }
    }

    // Straight delegation to the real console view

    public void scrollTo(int row)
    {
        consoleView.scrollTo(row);
    }

    @Override
    public void attachToProcess(ProcessHandler processHandler)
    {
        consoleView.attachToProcess(processHandler);
    }

    @Override
    public void setOutputPaused(boolean paused)
    {
        consoleView.setOutputPaused(paused);
    }

    @Override
    public boolean isOutputPaused()
    {
        return consoleView.isOutputPaused();
    }

    @Override
    public boolean hasDeferredOutput()
    {
        return consoleView.hasDeferredOutput();
    }

    @Override
    public void performWhenNoDeferredOutput(Runnable runnable)
    {
        consoleView.performWhenNoDeferredOutput(runnable);
    }

    @Override
    public void setHelpId(String helpId)
    {
        consoleView.setHelpId(helpId);
    }

    @Override
    public void addMessageFilter(com.intellij.execution.filters.Filter filter)
    {
        consoleView.addMessageFilter(filter);
    }

    @Override
    public int getContentSize()
    {
        return consoleView.getContentSize();
    }

    @Override
    public boolean canPause()
    {
        return consoleView.canPause();
    }

    @NotNull
    @Override
    public AnAction[] createConsoleActions()
    {
        return consoleView.createConsoleActions();
    }

    @Override
    public void allowHeavyFilters()
    {
        consoleView.allowHeavyFilters();
    }

    @Override
    public JComponent getPreferredFocusableComponent()
    {
        return consoleView.getPreferredFocusableComponent();
    }

    @Override
    public void dispose()
    {
        consoleView.dispose();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$()
    {
        rootPanel = new JPanel();
        rootPanel.setLayout(new FormLayout("fill:4dlu:noGrow,fill:d:grow,fill:3dlu:noGrow", "center:2dlu:noGrow,fill:d:noGrow,top:2dlu:noGrow,center:max(d;4px):grow"));
        headerPanel = new JPanel();
        headerPanel.setLayout(new FormLayout("fill:d:noGrow,left:4dlu:noGrow,fill:d:grow,left:4dlu:noGrow,fill:d:noGrow", "center:d:noGrow"));
        CellConstraints cc = new CellConstraints();
        rootPanel.add(headerPanel, cc.xy(2, 2));
        filterLabel = new JLabel();
        filterLabel.setIcon(new ImageIcon(getClass().getResource("/general/filter.png")));
        filterLabel.setText("");
        filterLabel.setToolTipText("Optional regular expression-based filter.");
        headerPanel.add(filterLabel, cc.xy(1, 1));
        filterTextField = new JTextField();
        filterTextField.setText("");
        filterTextField.setToolTipText("Optional regular expression-based filter.");
        headerPanel.add(filterTextField, cc.xy(3, 1));
        consoleViewPanel = new JPanel();
        consoleViewPanel.setLayout(new BorderLayout(0, 0));
        rootPanel.add(consoleViewPanel, cc.xy(2, 4, CellConstraints.DEFAULT, CellConstraints.FILL));
        filterLabel.setLabelFor(filterTextField);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$()
    {
        return rootPanel;
    }

    private static class ConsoleViewEntry
    {
        private final String message;
        private final ConsoleViewContentType contentType;
        private final HyperlinkInfo hyperlinkInfo;
        private final boolean allowFiltering;

        public ConsoleViewEntry(@NotNull String message, @NotNull ConsoleViewContentType contentType, @Nullable HyperlinkInfo hyperlinkInfo, boolean allowFiltering)
        {
            this.message = message;
            this.contentType = contentType;
            this.hyperlinkInfo = hyperlinkInfo;
            this.allowFiltering = allowFiltering;
        }

        public ConsoleViewEntry(@NotNull String message, @NotNull ConsoleViewContentType contentType, boolean allowFiltering)
        {
            this(message, contentType, null, allowFiltering);
        }

        public ConsoleViewEntry(@NotNull String message, @NotNull HyperlinkInfo hyperlinkInfo, boolean allowFiltering)
        {
            this(message, ConsoleViewContentType.NORMAL_OUTPUT, hyperlinkInfo, allowFiltering);
        }

        @NotNull
        public String getMessage()
        {
            return message;
        }

        @NotNull
        public ConsoleViewContentType getContentType()
        {
            return contentType;
        }

        @Nullable
        public HyperlinkInfo getHyperlinkInfo()
        {
            return hyperlinkInfo;
        }

        public boolean isAllowFiltering()
        {
            return allowFiltering;
        }
    }
}
